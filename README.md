![Project Avatar](repo-icon.png)

# Base Project Template

This project template includes the baseline files that should be included in all of Cody Bolling's personal projects along with instructions on how to setup the project properly.

## Project Structure

```text
base-project-template
│ README.md
│ CHANGELOG.md
| .gitlab-ci.yml
| .gitignore
| repo-icon.png
│
└─ example-subfolder
    │ ...
...
```

## Roadmap

This project shares a Trello board called [Code Infrastructure](https://trello.com/b/SA5Pbz8b/code-infrastructure) with [Base Pipeline](https://gitlab.com/cody.bolling/base-pipeline) and [CICD Toolkit](https://gitlab.com/cody.bolling/cicd-toolkit) because these projects do not undergo enough changes or backlog entries to warrant having their own boards.

## Project Setup

### Install Template

Download the Base Project Template, create a blank project in GitLab, and setup Git.

- Download [Base Project Template](https://gitlab.com/cody.bolling/base-project-template/-/archive/main/base-project-template-main.zip)
  - Unzip the downloaded .zip file, rename the unzipped folder to the name of your new project, and move the project folder to a proper location (i.e. Documents/git-repos/)

- [Create a new project in GitLab](https://gitlab.com/projects/new#blank_project). Set the name of your project, make it public, and **DO NOT INITIALIZE WITH A README**.

- Setup Git

  ```console
  cd path-to-project/
  git init --initial-branch=main
  git remote add origin git@gitlab.com:cody.bolling/project-name.git
  ```

- Edit and create a new repo-icon.png

### Set GitLab Settings

In GitLab, set various settings so the project functions as expected.

- Setting > General
  - Naming, topics, avatar
    - Set project description
    - Upload repo-icon.png as the project avatar
  - Visibility, project features, permissions
    - Uncheck Users can request access
    - Flexible tool to collaboratively develop ideas and plan work in this project. > Only Project Members
    - Submit changes to be merged upstream. > Only Project Members
    - Users can copy the repository to a new project. > Only Project Members
    - Build, test, and deploy your changes. > Only Project Members
    - Every project can have its own space to store its Docker images > Only Project Members
    - Requirements management system. > Only Project Members
    - Configure your project resources and monitor their health. > Only Project Members
    - Uncheck Disable email notifications
  - Merge requests
    - Uncheck Enable "Delete source branch" option by default
    - Set Default description template for merge requests to

      ```text
      - [ ] Update VERSION in .gitlab-ci.yml
      - [ ] Update CHANGELOG to reflect changes
      ```

- Settings > CICD
  - General pipelines
    - Uncheck Public pipelines
  - Variables
    - Add AWS_ACCESS_KEY_ID, uncheck Protected, and check Masked
    - Add AWS_SECRET_ACCESS_KEY, uncheck Protected, and check Masked
    - Add AWS_DEFAULT_REGION, uncheck Protected, and check Masked

### Initial Commit

Edit the README and perform the initial commit.

- Edit the project's README to reflect the given project. The README should have the following:
  - Title and description
  - Project Structure (file structure of the project)
  - Roadmap (Link to Trello Board and ongoing status of the project)
  - Add any additional important information relevant to the given project
- Run the initial commit to the main branch and push it to the origin

  ```console
  git add .
  git commit -m "Initial commit"
  git push origin main
  ```

### Final Steps

Create a dev branch to begin working on your project and some recommendations.

- Create a dev branch and push it to the origin
  
  ```console
  git checkout -b dev
  git push origin dev
  ```

- Recommend using Git Kraken to manage Git as the command line can get confusing at times.

### You're all set! Happy Coding!
