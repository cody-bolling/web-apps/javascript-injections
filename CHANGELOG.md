# CHANGELOG

## 0.2

- Update typos and structure in README and add TAGS variable to .gitlab-ci

## 0.1

- Initial Commit
